#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <iostream>
#include <list>
#include <vector>
#include <QPoint>

using namespace cv;

std::vector<QPoint> searchNeighbor(Mat* res, int i,int j);

Mat painting(std::vector<std::vector<QPoint>> pixcor,Mat res){

    RNG rng(12345);
    Mat result;
    cvtColor(res,result, COLOR_GRAY2RGB);

    for(int i =0;i < (int)pixcor.size();i++){
        Scalar color = Scalar(rng.uniform(0,255), rng.uniform(0, 255), rng.uniform(0, 255));
        for(int j=0;j < (int)pixcor.at(i).size(); j++){
            result.at<Vec3b>(pixcor.at(i).at(j).x(),pixcor.at(i).at(j).y())[0]=color.val[0];
            result.at<Vec3b>(pixcor.at(i).at(j).x(),pixcor.at(i).at(j).y())[1]=color.val[1];
            result.at<Vec3b>(pixcor.at(i).at(j).x(),pixcor.at(i).at(j).y())[2]=color.val[2];
        }
    }
    return result;

}


Mat searchPix(Mat binaryImage){

    Mat result;
    binaryImage.copyTo(result);
    std::vector<std::vector<QPoint>> pixelcor;

    int rows=binaryImage.rows;
    int cols=binaryImage.cols;

    for(int i=0;i<rows;i++){
        for(int j=0;j<cols;j++)
            if(result.at<uchar>(i,j)==0) pixelcor.push_back(searchNeighbor(&result,i,j));
    }
    return  painting(pixelcor,result);
}
std::vector<QPoint> searchNeighbor(Mat* res, int i,int j){

    std::vector<QPoint> objectcor;

    for(;;){

        if(res->at<uchar>(i,j)==0)
        {
            objectcor.push_back(QPoint(i,j));
            res->at<uchar>(i,j)=100;
            j++;
            continue;
        }

        if(res->at<uchar>(i,j+1)==0)
        {
            objectcor.push_back(QPoint(i,j+1));
            res->at<uchar>(i,j+1)=100;
            j++;
            continue;
        }
        if(res->at<uchar>(i+1,j)==0)
        {
            objectcor.push_back(QPoint(i+1,j));
            res->at<uchar>(i+1,j)=100;
            i++;
            continue;
        }
        if(res->at<uchar>(i-1,j)==0)
        {
            objectcor.push_back(QPoint(i-1,j));
            res->at<uchar>(i-1,j)=100;
            i--;
            continue;
        }
        if(res->at<uchar>(i,j-1)==0)
        {
            objectcor.push_back(QPoint(i,j-1));
            res->at<uchar>(i,j-1)=100;
            j--;
            continue;
        }


         if(res->at<uchar>(i+1,j+1)==0)
        {
            objectcor.push_back(QPoint(i+1,j+1));
            res->at<uchar>(i+1,j+1)=100;
            j++;
            i++;
            continue;
        }
        if(res->at<uchar>(i+1,j-1)==0)
        {
            objectcor.push_back(QPoint(i+1,j-1));
            res->at<uchar>(i+1,j-1)=100;
            i++;
            j--;
            continue;
        }
        if(res->at<uchar>(i-1,j+1)==0)
        {
            objectcor.push_back(QPoint(i-1,j+1));
            res->at<uchar>(i-1,j+1)=100;
            i--;
            j++;
            continue;
        }
        if(res->at<uchar>(i-1,j-1)==0)
        {
            objectcor.push_back(QPoint(i-1,j-1));
            res->at<uchar>(i-1,j-1)=100;
            --j;
            --i;
            continue;
        }
        break;
    }
    return objectcor;
}

int countObject(Mat binaryImage){

    int countOuterAngle =0;
    int countInnerAngle =0;

    int outMask[4][4] = {
        { 0,0,0,255 },
        { 0,0,255,0 },
        { 0,255,0,0 },
        { 255,0,0,0 }
    };
    int innerMask[4][4] = {
        { 255,255,255,0 },
        { 255,255,0,255 },
        { 255,0,255,255},
        { 0,255,255,255}
    };
    int rows=binaryImage.rows;
    int cols=binaryImage.cols;

    for(int i=0;i<rows-1;i++)
        for(int j=0;j<cols-1;j++)
            for(int k =0; k<4;k++){
                if((binaryImage.at<uchar>(i,j)==outMask[k][0]) and (binaryImage.at<uchar>(i,j+1)==outMask[k][1]))
                    if((binaryImage.at<uchar>(i+1,j)==outMask[k][2]) and (binaryImage.at<uchar>(i+1,j+1)==outMask[k][3]))
                    {
                        countOuterAngle++;
                        break;
                    }
                if((binaryImage.at<uchar>(i,j)==innerMask[k][0]) and (binaryImage.at<uchar>(i,j+1)==innerMask[k][1]))
                    if((binaryImage.at<uchar>(i+1,j)==innerMask[k][2]) and (binaryImage.at<uchar>(i+1,j+1)==innerMask[k][3]))
                    {
                        countInnerAngle++;
                        break;
                    }
            }
    int Q = (countOuterAngle-countInnerAngle)/4;
    return Q;
}

Mat getTreshHoldImage(Mat grayScaleImage){

    Mat binaryImage = grayScaleImage.clone();
    int rows=grayScaleImage.rows;
    int cols=grayScaleImage.cols;

    for(int i=0;i<rows;i++){
        for(int j=0;j<cols;j++){
            if(binaryImage.at<uchar>(i,j) > 180)
                binaryImage.at<uchar>(i,j) = 255;
            else
            {
                binaryImage.at<uchar>(i,j) = 0;
            }
        }
    }
    return binaryImage;
}

Mat grayScale(Mat RGBImage){

    Mat grayScaleImage(RGBImage.size(),CV_8UC1);

    int rows=RGBImage.rows;
    int cols=RGBImage.cols;

    for(int i=0;i<rows;i++){
        for(int j=0;j<cols;j++){
            Vec3b intensity=RGBImage.at<Vec3b>(i,j);
            int blue=intensity.val[0];
            int green =intensity.val[1];
            int red=intensity.val[2];
            grayScaleImage.at<uchar>(i,j)=blue*0.0722+green*0.7152+red*0.2126;
        }
    }

    return grayScaleImage;
}

int main()
{
    Mat RGBImage;
    Mat grayScaleImage;
    Mat binaryImage;

    RGBImage =imread("test1.jpg");

    grayScaleImage = grayScale(RGBImage);

    binaryImage = getTreshHoldImage(grayScaleImage);



    int res=countObject(binaryImage);
    String result = "Всего объектов "+std::to_string(res);



    namedWindow("RGBImage",1);
    namedWindow("GrayScaleImage",1);
    namedWindow("Binary",1);
    namedWindow(result,1);

    imshow("RGBImage",RGBImage);
    imshow("GrayScaleImage",grayScaleImage);
    imshow("Binary",binaryImage);
    imshow(result,searchPix(binaryImage));

    waitKey();
    return 0;

}
