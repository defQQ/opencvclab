

import numpy as np

np.random.seed(1)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def deriv(x):
    return x * (1 - x)


def gen_network(network_shape):
    array_ws = []
    for i in range(0, len(network_shape) - 1):
        cur_idx = i
        next_idx = i + 1

        array_w = 2*np.random.rand(network_shape[next_idx], network_shape[cur_idx]) - 1
        array_ws.append(array_w)

    return array_ws


def run_network(input, network_shape, nn_weights):
    current_input = input
    outputs = []
    for nn_weight in nn_weights:
        temp_out_cur = np.dot(nn_weight, current_input)
        output = sigmoid(temp_out_cur)
        outputs.append(output)
        current_input = output

    return output.T


def train_main(input, output, rate_tr, network_shape, nn_weights):

    array_ws = nn_weights
    for i in range(10000):
        array_ws = train(input, output, rate_tr, network_shape, array_ws)
    return array_ws


def train(input, output, rate_tr, shape, nn_weights):
    current_input = input
    outputs = []
    for nn_weight in nn_weights:
        temp_out_cur = np.dot(nn_weight, current_input)
        output = sigmoid(temp_out_cur)
        outputs.append(output)
        current_input = output


    deltas = []


    error = output - outputs[len(outputs)-1]
    delta = error * deriv(outputs[len(outputs)-1])
    deltas.append(delta)

    cur_delta = delta
    back_idx = len(outputs) - 2


    for nn_weight in nn_weights[::-1][:-1]:
        next_error = np.dot(nn_weight.T, cur_delta)
        next_delta = next_error * deriv(outputs[back_idx])
        deltas.append(next_delta)
        cur_delta = next_delta
        back_idx -= 1

    val_weight_dx = len(nn_weights) - 1

    for delta in deltas:
        input_used = None
        if val_weight_dx - 1 < 0:
            input_used = input
        else:
            input_used = outputs[val_weight_dx - 1]

        nn_weights[val_weight_dx] += rate_tr*np.dot(delta, input_used.T)
        val_weight_dx -= 1


    return nn_weights