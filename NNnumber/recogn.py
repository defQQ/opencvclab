
from Network import gen_network, train_main, run_network
import numpy as np
from PIL import Image
import copy


def normalize_image(imgArr):

    newImgArr = copy.deepcopy(imgArr)
    for row in range(len(imgArr)):
        for col in range(len(imgArr[row])):
            new_value = (int(imgArr[row][col][0]) + int(imgArr[row][col][1]) + int(imgArr[row][col][2])) / 3
            if new_value != 0.0:

                new_value = 255.0
            newImgArr[row][col][0] = new_value
            newImgArr[row][col][1] = new_value
            newImgArr[row][col][2] = new_value

    return newImgArr


def generate_dataset():

    imgArrList = []
    numbersRange = range(0, 10)
    numbersVers = range(1, 16)
    outputs = []

    for number in numbersRange:
        for version in numbersVers:
            cur_output = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
            cur_output[int(number)] = 1.0
            outputs.append(cur_output)
            imgPath = 'images/numbers/' + str(number) + '.' + str(version) + '.png'
            imgCur = Image.open(imgPath)

            imageC = normalize_image(np.array(imgCur))
            imgArrList.append(imageC)

    inputs = []
    for imgArr in imgArrList:
        pixels = []
        for row in imgArr:
            for col in row:

                pixels.append(col[0]/255.0)
        inputs.append(pixels)

    return [inputs, outputs]


def generate_test_data(imgPath):

    imgCur = Image.open(imgPath)
    imageC = normalize_image(np.array(imgCur))
    inputs = []
    pixels = []
    for row in imageC:
        for col in row:
            pixels.append(col[0]/255.0)
    inputs.append(pixels)
    return np.array(inputs).T



data = generate_dataset()

inputs = np.array(data[0]).T
outputs = np.array(data[1]).T


shape = [64, 32, 10]

weights = gen_network(shape)
weights = train_main(inputs, outputs, 0.01, shape, weights)


for i in range(10):
    inputT = generate_test_data('images/test_numbers/test' + str(i) + '.png')
    test_outputs = run_network(inputT, shape, weights)

    print("Test " + str(i+1) + ": Input - " + str(i) + ", Output - ")
    for output in test_outputs:
        best_match = max(output)
        for idx in range(len(output)):
            if output[idx] == best_match:
                print (idx)

