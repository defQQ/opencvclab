#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <iostream>
#include <vector>
#include <QPoint>

using namespace cv;

Mat upBuilding(Mat src,Mat mask){

    Mat res= src.clone();

    int centerX = mask.rows/2;
    int centerY = mask.cols/2;

    int rows_src=src.rows;
    int colums_src = src.cols;

    for(int i=0;i<rows_src-mask.rows;i++)
        for(int j=0;j<colums_src-mask.cols;j++)
        {
            if(res.at<uchar>(i,j)==0){
                for(int k=-centerX; k < centerX; k++){
                    for(int m=-centerY;m < centerY; m++){
                        if((i+k >0) and (j+m >0)){
                            //   if(mask.at<uchar>(abs(k),abs(m))==0)
                            res.at<uchar>(i+k,j+m)=mask.at<uchar>(centerX+k,centerY+m);
                            /*(mask.at<uchar>(centerX+k,centerY+m))+
                                    src.at<uchar>(i+k,j+m);
                            if(res.at<uchar>(i+k,j+m)!=0) res.at<uchar>(i+k,j+m)=0;*/
                        }
                    }
                }
            }
        }

    return res;
}


Mat erode(Mat src, Mat mask){

    Mat res(src.rows,src.cols,src.type(),Scalar(255));

    QPoint centerMask(mask.rows/2,mask.cols/2);

    int rows_src=src.rows;
    int colums_src = src.cols;
    bool check;

    for(int i=0;i<rows_src - mask.rows;i++)
        for(int j=0;j<colums_src-mask.cols;j++)
        {
            check = true;
            for(int k=0; k < mask.rows; k++){
                for(int m=0;m < mask.cols; m++)
                    if(mask.at<uchar>(k,m)==0){
                        if(mask.at<uchar>(k,m)!=src.at<uchar>(i+k,j+m)){
                            check=false;
                            break;
                        }

                    }

            }
            if(check) res.at<uchar>(i+centerMask.x(),j+centerMask.y())=0;
        }

    return res;
}


int main()
{
    Mat src= imread("src.bmp");
    Mat mask1= imread("mask55.bmp");
    Mat temp;
    Mat mask2 = imread("mask3.bmp");
    Mat mask3 = imread("mask4.bmp");
    Mat mask4 = imread("mask2.bmp");
    Mat mask5 = imread("mask66.bmp");
    Mat mask6 = imread("mask1.bmp");
    Mat mask7 = imread("mask7.bmp");
    Mat mask8 = imread("mask5.bmp");
    cvtColor(src, temp, CV_BGR2GRAY);
    threshold( temp, src, 180,255,THRESH_BINARY );

    cvtColor(mask1, temp, CV_BGR2GRAY);
    threshold( temp, mask1, 180,255,THRESH_BINARY );

    namedWindow("Binary",1);
    namedWindow("Result",1);
    //namedWindow("mask",1);
    //замыкание
    // temp = upBuilding(src,mask2);
    // temp = erode(temp,mask3);

    //размыкание
    //temp = erode(src,mask5);
    //temp = upBuilding(temp,mask2);

    //прямоугольник
     temp = erode(src,mask2);
     temp = upBuilding(temp,mask6);

    //круг
 /*   temp = erode(src,mask5);
    temp = erode(temp,mask5);
    temp = erode(temp,mask5);
    temp = upBuilding(temp,mask1);
    temp = upBuilding(temp,mask4);*/

    //треугольник
  /*  temp = erode(src,mask5);
    temp = erode(temp,mask5);
    temp = erode(temp,mask5);
    temp = upBuilding(temp,mask7);
    temp = upBuilding(temp,mask7);*/

//    temp = erode(temp,mask5);


    imshow("Binary",src);
    imshow("Result",temp);
    //  imshow("mask",mask1);

    waitKey();
    return 0;

}
