#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <iostream>
#include <vector>
#include <QPoint>

using namespace cv;



int main()
{
    Mat src= imread("test.jpg");
    Mat gray;

    cvtColor(src, gray, CV_BGR2GRAY);
    threshold( gray, src, 150,255,THRESH_BINARY );



    namedWindow("Binary",1);
    namedWindow("Erode",1);


    imshow("Binary",src);
    imshow("Erode",gray);


    waitKey();
    return 0;

}
